<?php
/*
Plugin Name: ACF Flex Columns
Description: Replace the regular editor with responsive multiple column editors.  Requires ACF Pro, then be sure to import the linked JSON Import File under Custom Fields > Tools
Version: 2.1.8
Author: imageDESIGN
Author URI:  http://imagedesign.pro
Text Domain: acf-flex-columns
GitLab Plugin URI: https://gitlab.com/idp-wp-plugins/acf-flex-columns
*/
include_once( ABSPATH . 'wp-includes/pluggable.php' );
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
class IDP_ACF_FlexCols{
    private $options;
    
	public function __construct(){
		    add_action('plugin_action_links_' . plugin_basename( __FILE__ ), array($this, 'plugin_action_links'));
        if(  is_plugin_active( 'acf-flexible-columns-pro/acf-flexible-columns-pro.php' ) ):
            //ALERT and DEACTIVATE - OLD Pro version is still active
            add_action( 'admin_init', array($this, 'plugin_deactivate2') );
            add_action( 'admin_notices', array( $this, 'plugin_admin_notice2') );
        elseif(  is_plugin_active( 'advanced-custom-fields-pro/acf.php' ) ):
            //Migrate Content on Activation
            /*if ( isset($_GET['acffc_nonce']) && wp_verify_nonce($_GET['acffc_nonce'], 'migrate_content')){
                add_action( 'init', array( $this, 'migrate_thecontent'));
                add_action( 'admin_notices', array($this, 'admin_notice') );
            } */
            //Options Page
            add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
            add_action( 'admin_init', array( $this, 'page_init' ) );
			//Remove Regular Content Editor
			add_action('admin_head', array($this, 'noeditor') );
            //Check if Synced and Set Option defaults
            if( get_option('acf_flexcol') == 'synced' && !$this->SyncCheck() ) 
                delete_option('acf_flexcol');
			if( get_option('acf_flexcol') != 'synced' ) 
                add_filter('acf/settings/load_json', array($this, 'acf_json_load_point') );
            if( get_option('acf_flexcol') != 'synced' && $this->SyncCheck() ) {
                update_option('acf_flexcol', 'synced'); 
                update_option('acf_flexcol_opts', array( 'loadboots' => 1, 'hideposteditor' => 1, 'hidepageeditor' => 1 )); 
            }
            //Override the_content() to display columns
			add_filter('the_content', array($this, 'new_the_content') );
            //Bootstrap Grid CSS
			add_action('wp_enqueue_scripts', array($this, 'styles_and_scripts') );
            //LI Column CSS
            add_action('wp_head', array($this, 'li_columns') );
            //LI Column Editor Styles
            add_action( 'admin_init', array($this, 'add_editor_style') );
            //LI Column Editor Scripts
            add_action( 'after_wp_tiny_mce', array($this, 'add_editor_scripts') );
        
            //Carousel Image Sizes
            add_image_size( 'Mobile', 768, 1024 );
            add_image_size( 'Tablet', 992, 1200 );
            add_image_size( 'Desktop', 1280, 1080 );
            add_image_size( 'Widescreen', 2000, 1280 );

            //Flex CSS for admin column layout in editor
            add_action('admin_head', array($this, 'flex_columns_admin_css'));

            //Default Container Wrapper Options
            add_filter( 'flexible_columns_wrap_outer', array($this,'container_options_wrap_outer'));
            add_filter( 'flexible_columns_wrap_outer_end', array($this,'container_options_wrap_outer_end'));
            
		else:
			//ALERT - no ACF PRO	
			//add_action( 'admin_init', array($this, 'plugin_deactivate') );
			add_action( 'admin_notices', array( $this, 'plugin_admin_notice') );
		endif;
	}
	
  function plugin_action_links( $links ){
    $dl = plugin_dir_url( __FILE__  ).'assets/acf-flex-column-groups.json';
    $links = array_merge( array( '<a href="'.$dl.'" target="_blank">JSON Field Groups Import File</a>'), $links);
    return $links;
  }
  
	function plugin_deactivate(){
		deactivate_plugins( plugin_basename( __FILE__ ) );	
	}
	
	function plugin_admin_notice(){
        $url = 'https://www.advancedcustomfields.com/pro/';
		echo '<div class="updated"><p><strong>'.__('ACF Flexible Columns', 'acf-flex-columns').'</strong> '.sprintf( wp_kses( __('requires the <a href="%s" target="_blank">Advanced Custom Fields Pro</a> plugin in order to function; the plug-in has been <strong>deactivated</strong>', 'acf-flex-columns'), array( 'a' => array( 'href' => array() ) ) ), esc_url( $url ) ).'</p></div>';
               if ( isset( $_GET['activate'] ) )
                    unset( $_GET['activate'] );
	}
    function plugin_deactivate2(){
        $oldplugin = 'acf-flexible-columns-pro/acf-flexible-columns-pro.php';
		deactivate_plugins( $oldplugin );	
	}
    function plugin_admin_notice2(){
		echo '<div class="updated"><p><strong>'.__('ACF Flexible Columns', 'acf-flex-columns').'</strong> '. __(' is now activated.  ACF Flexible Columns PRO has been deactivated to prevent conflicts.', 'acf-flex-columns').'</p></div>';
               if ( isset( $_GET['activate'] ) )
                    unset( $_GET['activate'] );
	}
    
    function migrate_thecontent(){
        global $wpdb;
         if (isset($_GET['acffc_nonce']) && wp_verify_nonce($_GET['acffc_nonce'], 'migrate_content')):
        $args = array(
            'post_type' => array( 'post', 'page'),
            'posts_per_page' => -1
        );
        $query = new WP_Query( $args );
        if( $query->have_posts() ):
            while( $query->have_posts() ):
                $query->the_post();
                $oldContent = get_the_content();
                $field_key = "field_574881bf81a25";
                update_post_meta($query->post->ID, '_rows', $field_key );
                update_post_meta($query->post->ID, 'rows', array ('full_width_row') );
                update_post_meta($query->post->ID, '_rows_0_column_1_block', 'field_574881bf9302c' );
                update_post_meta($query->post->ID, 'rows_0_column_1_block', array('content') );
                update_post_meta($query->post->ID, '_rows_0_column_1_block_0_list_layout', 'field_574881bf9dd99' );
                update_post_meta($query->post->ID, 'rows_0_column_1_block_0_list_layout', 1 );
                update_post_meta($query->post->ID, '_rows_0_column_1_block_0_content', 'field_574881bf9dd23' );
                update_post_meta($query->post->ID, 'rows_0_column_1_block_0_content', $oldContent );
            endwhile;
        endif;
       wp_reset_postdata();
        endif;
    }
    
    function add_plugin_page(){
        add_options_page(
            //'edit.php?post_type=acf-field-group',
            'ACF Flexible Columns', 
            'Flexible Columns', 
            'manage_options', 
            'acf-flex-columns', 
            array( $this, 'create_admin_page' )
        );
    }
    
    function create_admin_page(){
        $this->options = get_option( 'acf_flexcol_opts' );
        ?>
        <div class="wrap">
            <h2>ACF Flexible Columns Settings</h2>           
            <form method="post" action="options.php">
            <?php
                // This prints out all hidden setting fields
                settings_fields( 'acf_flexcol_options' );   
                do_settings_sections( 'acf-fc-setting-admin' );
                submit_button(); 
            ?>
            </form>
            <?php include_once('layout-function-samples.html'); ?>
        </div>
        <?php
    }
    
    function page_init(){
        register_setting(
            'acf_flexcol_options', // Option group
            'acf_flexcol_opts', // Option name
            array( $this, 'sanitize' ) // Sanitize
        );
        
        add_settings_section(
            'acffxin', // ID
            __('Content Migration', 'acf-flex-columns'), // Title
            array( $this, 'print_section_info' ), // Callback
            'acf-fc-setting-admin' // Page
        ); 
        
        add_settings_field(
            'migrate', // ID
            __('Migrate Content', 'acf-flex-columns'), // Title 
            array( $this, 'migrate_button' ), // Callback
            'acf-fc-setting-admin', // Page
            'acffxin' // Section           
        ); 
       
        add_settings_section(
            'acffxopts', // ID
            __('Default Overrides', 'acf-flex-columns'), // Title
            '',//array( $this, 'print_section_info' ), // Callback
            'acf-fc-setting-admin' // Page
        );  

        add_settings_field(
            'bootstrap', // ID
            __('Load Bootstrap Grid CSS', 'acf-flex-columns'), // Title 
            array( $this, 'load_bootstrap' ), // Callback
            'acf-fc-setting-admin', // Page
            'acffxopts' // Section           
        ); 
        
        add_settings_field(
            'aos', // ID
            __('Load AOS Library', 'acf-flex-columns'), // Title 
            array( $this, 'load_aos' ), // Callback
            'acf-fc-setting-admin', // Page
            'acffxopts' // Section           
        ); 
        
        add_settings_field(
            'slick', // ID
            __('Load Slick Library', 'acf-flex-columns'), // Title 
            array( $this, 'load_slick' ), // Callback
            'acf-fc-setting-admin', // Page
            'acffxopts' // Section           
        ); 

        add_settings_field(
            'hideposteditor', 
            __('Hide Regular Post Content Editor', 'acf-flex-columns'), 
            array( $this, 'hide_post_editor' ), 
            'acf-fc-setting-admin', 
            'acffxopts'
        );  
        
         add_settings_field(
            'hidepageeditor', 
            __('Hide Regular Page Content Editor', 'acf-flex-columns'), 
            array( $this, 'hide_page_editor' ), 
            'acf-fc-setting-admin', 
            'acffxopts'
        );  
		
	/*	add_settings_field(
            'enablecontainers', 
            __('Enable Row Containers', 'acf-flex-columns'), 
            array( $this, 'enable_containers' ), 
            'acf-fc-setting-admin', 
            'acffxopts'
        );  
		*/
		add_settings_field(
            'disablelicolstyles', 
            __('Disable List Column Styles', 'acf-flex-columns'), 
            array( $this, 'disable_licol_styles' ), 
            'acf-fc-setting-admin', 
            'acffxopts'
        );
        
        add_settings_field(
            'disablecontentlayout', 
            __('Disable Default content type layout ', 'acf-flex-columns'), 
            array( $this, 'disable_content_layout' ), 
            'acf-fc-setting-admin', 
            'acffxopts'
        );
    }
    
    function print_section_info(){
        
    }
    function admin_notice(){

            ?>
            <div class="notice notice-success is-dismissible">
        <p><?php _e( 'Previous page content has been successfully migrated to the Flexible Column system!', 'acf-flixible-columns' ); ?></p>
    </div><?php

        
    }
    function sanitize( $input ){
        $new_input = array();
        if( isset( $input['bootstrap'] ) )
            $new_input['bootstrap'] = absint( $input['bootstrap'] );
        
        if( isset( $input['aos'] ) )
            $new_input['aos'] = absint( $input['aos'] );
        
        if( isset( $input['slick'] ) )
            $new_input['slick'] = absint( $input['slick'] );

        if( isset( $input['hideposteditor'] ) )
            $new_input['hideposteditor'] = sanitize_text_field( $input['hideposteditor'] );
        
        if( isset( $input['hidepageeditor'] ) )
            $new_input['hidepageeditor'] = sanitize_text_field( $input['hidepageeditor'] );
		
		/*if( isset( $input['enablecontainers'] ) )
            $new_input['enablecontainers'] = sanitize_text_field( $input['enablecontainers'] );
		*/
		if( isset( $input['disablelicolstyles'] ) )
            $new_input['disablelicolstyles'] = sanitize_text_field( $input['disablelicolstyles'] );

        if( isset( $input['disablecontentlayout'] ) )
            $new_input['disablecontentlayout'] = sanitize_text_field( $input['disablecontentlayout'] );

        return $new_input;
    }
    
    function migrate_button(){
        echo '<p><a href="'.wp_nonce_url(admin_url('options-general.php?page=acf-flex-columns'), 'migrate_content', 'acffc_nonce').'" class="button button-secondary">Migrate to Column System</a><br><small>This will copy over your previous page content into the new column content system</small></p>';
    }
    
    function load_bootstrap(){
        printf(
            '<label><input type="checkbox" id="bootstrap" name="acf_flexcol_opts[bootstrap]" value="1" %s /> Bootstrap Grids</label>',
            isset( $this->options['bootstrap'] ) ? esc_attr( 'checked="checked"') : ''
        );
        echo '<p><small>'.__('Columns rely on Bootstrap 4 grid styles to function, if you already have Bootstrap 4 styles loaded in your template you can safely disable loading Bootstrap. (Note this CSS file only contains the basic grid functionality from Bootstrap, not the entire library)', 'acf-flex-columns').'</small><p>';
    }
    function load_aos(){
        printf(
            '<label><input type="checkbox" id="aos" name="acf_flexcol_opts[aos]" value="1" %s /> AOS (Animated On Scroll) Library CSS & JS</label>',
            isset( $this->options['aos'] ) ? esc_attr( 'checked="checked"') : ''
        );
        echo '<p><small>'.__('Animations on each column block rely on the AOS Library to function.  If you are already including this library in your theme you may safely disable loading these files.', 'acf-flex-columns').'</small><p>';
    }
    function load_slick(){
        printf(
            '<label><input type="checkbox" id="aos" name="acf_flexcol_opts[slick]" value="1" %s /> Slick Library CSS & JS</label>',
            isset( $this->options['slick'] ) ? esc_attr( 'checked="checked"') : ''
        );
        echo '<p><small>'.__('The photo carousel relies on the Slick Library to function.  If you are already including this library in your theme you may safely disable loading these files.', 'acf-flex-columns').'</small><p>';
    }
    
    function hide_post_editor(){
        printf(
            '<input type="checkbox" id="hideposteditor" name="acf_flexcol_opts[hideposteditor]" value="1" %s />',
            isset( $this->options['hideposteditor'] ) ? esc_attr( 'checked="checked"') : ''
        );
         echo '<p><small>'.__('The regular Post editor is replaced by the column editor, if you need it back for any reason, uncheck this box', 'acf-flex-columns').'</small><p>';
    }
    
    function hide_page_editor(){
        printf(
            '<input type="checkbox" id="hidepageeditor" name="acf_flexcol_opts[hidepageeditor]" value="1" %s />',
            isset( $this->options['hidepageeditor'] ) ? esc_attr( 'checked="checked"') : ''
        );
        echo '<p><small>'.__('The regular Page editor is replaced by the column editor, if you need it back for any reason, uncheck this box', 'acf-flex-columns').'</small><p>';
    }
	
	/*function enable_containers(){
        printf(
            '<input type="checkbox" id="enablecontainers" name="acf_flexcol_opts[enablecontainers]" value="1" %s />',
            isset( $this->options['enablecontainers'] ) ? esc_attr( 'checked="checked"') : ''
        );
        echo '<p><small>'.__('You can now add .container & .container-fluid around each row, if upgrading, add a \'container\' field above the first Column in each main Layout where 1 = .container and 0 = .container-fluid', 'acf-flex-columns').'</small><p>';
    }*/
	function disable_licol_styles(){
        printf(
            '<input type="checkbox" id="disablelicolstyles" name="acf_flexcol_opts[disablelicolstyles]" value="1" %s />',
            isset( $this->options['disablelicolstyles'] ) ? esc_attr( 'checked="checked"') : ''
        );
        echo '<p><small>'.__('Remove the embeded styles added to the page for the list item columns:', 'acf-flex-columns').'</small><p><code>.li-col-2{-webkit-column-count:2;-moz-column-count:2;column-count:2;}.li-col-3{-webkit-column-count:3;-moz-column-count:3;column-count:3;}.li-col-4{-webkit-column-count:4;-moz-column-count:4;column-count:4;}.li-col-5{-webkit-column-count:5;-moz-column-count:5;column-count:5;}.li-col-6{-webkit-column-count:6;-moz-column-count:6;column-count:6;}</code>';
    }

    function disable_content_layout(){
        printf(
            '<input type="checkbox" id="disablecontentlayout" name="acf_flexcol_opts[disablecontentlayout]" value="1" %s />',
            isset( $this->options['disablecontentlayout'] ) ? esc_attr( 'checked="checked"') : ''
        );
        echo '<p><small>'.__('Disable the built-in Content Block layout so you can override with your own code', 'acf-flex-columns');
    }
    
    function SyncCheck(){
        global $wpdb;
        $args = array(
            'name'          =>  'group_574881bf6b7db',
            'post_type'     =>  'acf-field-group',
            'post_status'   =>  'publish',
            'numberposts'   =>  1  
        );
        $synced = get_posts($args);
        if( $synced ) return true;
        else return false;
    }
	
	function noeditor(){
        $opts = get_option('acf_flexcol_opts');
		if( isset($opts['hideposteditor']) && $opts['hideposteditor'] == true) remove_post_type_support('post', 'editor');	
        if( isset($opts['hidepageeditor']) && $opts['hidepageeditor'] == true ) remove_post_type_support('page', 'editor');	
	}
	
	function acf_json_load_point( $paths ){
		$paths[] = 	plugin_dir_path(__FILE__).'acf-json/';
		return $paths;
	}
	
	function styles_and_scripts(){
		//CSS Styles
		$opts = get_option('acf_flexcol_opts');
        if( array_key_exists('bootstrap', $opts) && $opts['bootstrap'] ) wp_enqueue_style( 'BootstrapGrids', plugins_url('css/bootstrap.grids.min.css', __FILE__), 'all' );
        if( array_key_exists('aos', $opts) && $opts['aos'] ) wp_enqueue_style( 'AOS', plugins_url('css/aos.min.css', __FILE__), 'all' );
        if( array_key_exists('slick', $opts) && $opts['slick'] ) wp_enqueue_style( 'Slick', plugins_url('css/slick.css', __FILE__), 'all' );
        if( array_key_exists('slick', $opts) && $opts['slick'] ) wp_enqueue_style( 'Slick-Theme', plugins_url('css/slick-theme.css', __FILE__), 'all' );
		//JS Scripts
        if( array_key_exists('aos', $opts) && $opts['aos'] ) wp_enqueue_script( 'AOS', plugins_url('js/aos.min.js', __FILE__), array('jquery') );
        if( array_key_exists('slick', $opts) && $opts['slick'] ) wp_enqueue_script( 'Slick', plugins_url('js/slick.min.js', __FILE__), array('jquery') );
	}
    
    function li_columns(){
        $opts = get_option('acf_flexcol_opts');
        if( (!array_key_exists('bootstrap', $opts) || !$opts['bootstrap'] ) && (!array_key_exists('disablelicolstyles', $opts) || !$opts['disablelicolstyles'] )){
            ?>
<style type="text/css">
@media (min-width: 768px) {
.li-col-2{-webkit-column-count:2;-moz-column-count:2;column-count:2;}.li-col-3{-webkit-column-count:3;-moz-column-count:3;column-count:3;}.li-col-4{-webkit-column-count:4;-moz-column-count:4;column-count:4;}.li-col-5{-webkit-column-count:5;-moz-column-count:5;column-count:5;}.li-col-6{-webkit-column-count:6;-moz-column-count:6;column-count:6;}  
}   
</style>
            <?php
        }
    }
	
    function add_editor_scripts(){
        printf( '<script type="text/javascript" src="%s"></script>',  plugins_url('/js/tinymce-licols.js', __FILE__) );
    }
    
    function add_editor_style(){
        add_editor_style(plugins_url('css/editor-li-style.css', __FILE__) );
    }
    
	function new_the_content($content){
		global $post;
        $opts = get_option('acf_flexcol_opts');
        if( !$opts['hideposteditor'] && get_post_type( get_the_ID() ) == 'post' ){
            return $content;
        }
        $old_content = $content;
		$content = '';
		if( have_rows('rows') ):
		while( have_rows('rows') ): 
			the_row();
            $content .= apply_filters( 'flexible_columns_wrap_outer', false );
            $container = get_sub_field('container_options');
            $xtrarowclass = '';
            if( array_key_exists('additional_classes',$container) && $container['additional_row_classes'] ) $xtrarowclass = ' '.$container['additional_row_classes'];
			/*if( isset($opts['enablecontainers']) && $opts['enablecontainers'] == true):
                $nocontainer = get_sub_field('full_width_row'); 
				if( $nocontainer ){ $container = false; }
				if( $container == 1 ) $content .= '<div class="container">';	else $content .= '<div class="container-fluid">';
			endif;*/
			$content .= apply_filters( 'flexible_columns_wrap_inner', false );
            $rowclass = array('row');
            $rowclass = apply_filters('flexible_columns_row_class', $rowclass);
            $rowclasses = implode(' ', $rowclass).$xtrarowclass;
			
            if( get_row_layout() == 'full_width_row' ):
                
                $content .= apply_filters( 'flexible_column_pre', false );
                //SINGLE COLUMN
                $content .= '<div class="'.$rowclasses.'">'; 
                if( have_rows('column_1_block') ):
                    
					 while( have_rows('column_1_block') ): the_row();
											
                        $content .= apply_filters( 'flexible_columns_row_prepend', false );
						if( get_row_layout() ) $content .= $this->flex_col_layout(get_row_layout());
                        $content .= apply_filters( 'flexible_columns_row_append', false );					
					
					endwhile; 
                endif; 
                $content .=  '</div>';
				$content .= apply_filters( 'flexible_column_post', false );
				
			elseif( get_row_layout() == '2_column_row' ):
				
                $content .= apply_filters( 'flexible_column_pre', false );
                $content .=  '<div class="'.$rowclasses.'">';
                //TWO COLUMNS - COL #1
				if( have_rows('column_1_block') ):
					 while( have_rows('column_1_block') ): the_row();
						
                        $content .= apply_filters( 'flexible_columns_row_prepend', false );
						if( get_row_layout() ) $content .= $this->flex_col_layout(get_row_layout());
                    endwhile; 
                    
				endif;
				//TWO COLUMNS - COL #2
				if( have_rows('column_2_block') ):
					 while( have_rows('column_2_block') ): the_row();
						if( get_row_layout() ) $content .= $this->flex_col_layout(get_row_layout());
                       $content .=  apply_filters( 'flexible_columns_row_append', false );
						
					endwhile;	
                endif;
                $content .=  '</div><!-- /.row (2 col) -->';
				$content .= apply_filters( 'flexible_column_post', false );
				
			elseif( get_row_layout() == '3_column_row' ):
				
                $content .= apply_filters( 'flexible_column_pre', false );	
                $content .= '<div class="'.$rowclasses.'">';	
                //THREE COLUMNS - COL #1
				if( have_rows('column_1_block') ):
					 while( have_rows('column_1_block') ): the_row();
				        
                       $content .=  apply_filters( 'flexible_columns_row_prepend', false );
						if( get_row_layout() )$content .= $this->flex_col_layout(get_row_layout());
					endwhile; 
				endif;
				//THREE COLUMNS - COL #2	
				if( have_rows('column_2_block') ):
					 while( have_rows('column_2_block') ): the_row();
						if( get_row_layout() ) $content .= $this->flex_col_layout(get_row_layout());
					endwhile;
				endif;
				//THREE COLUMNS - COL #3	
				if( have_rows('column_3_block') ):
					 while( have_rows('column_3_block') ): the_row();
						if( get_row_layout() ) $content .= $this->flex_col_layout(get_row_layout());
                        $content .= apply_filters( 'flexible_columns_row_append', false );
						
					endwhile;
                endif;
                $content .= '</div> <!-- /.row -->';
               $content .= apply_filters( 'flexible_column_post', false );
				
			elseif( get_row_layout() == '4_column_row' ):
				
                $content .= apply_filters( 'flexible_column_pre', false );
                $content .= '<div class="'.$rowclasses.'">';
				//FOUR COLUMNS - COL #1
				if( have_rows('column_1_block') ):
					 while( have_rows('column_1_block') ): the_row();
				        
                       $content .=  apply_filters( 'flexible_columns_row_prepend', false );
						if( get_row_layout() ) $content .= $this->flex_col_layout(get_row_layout());
					endwhile; 
				endif;
				//FOUR COLUMNS - COL #2	
				if( have_rows('column_2_block') ):
					 while( have_rows('column_2_block') ): the_row();
						if( get_row_layout() ) $content .= $this->flex_col_layout(get_row_layout());
					endwhile;
				endif;
				//FOUR COLUMNS - COL #3	
				if( have_rows('column_3_block') ):
					 while( have_rows('column_3_block') ): the_row();
						if( get_row_layout() ) $content .= $this->flex_col_layout(get_row_layout());
					endwhile;
				endif;
				//FOUR COLUMNS - COL #4	
				if( have_rows('column_4_block') ):
					 while( have_rows('column_4_block') ): the_row();
						if( get_row_layout() ) $content .= $this->flex_col_layout(get_row_layout());
                        $content .= apply_filters( 'flexible_columns_row_append', false );
						
					endwhile;
                endif;
                $content .= '</div> <!-- /.row -->';
				$content .= apply_filters( 'flexible_columns_post', false );	
			
			endif;	
			$content .= apply_filters( 'flexible_columns_wrap_inner_end', false );
			//if( isset($opts['enablecontainers']) && $opts['enablecontainers'] == true) $content .= '</div>'; //End .container/container-fluid
			$content .= apply_filters( 'flexible_columns_wrap_outer_end', false );
        endwhile; //end rows
        else:
            $content = $old_content;
	    endif;
		
		if ( !post_password_required() ) {
			return apply_filters( 'flexible_columns_content', $content );
		}else{
			return get_the_password_form();	
		}
    }
    
    function column_widths($width, $classes=false){
        $xl = ''; $lg = ''; $md = ''; $sm = ''; $xs = '';
        if( !$width || $width == 'auto' ):
            $col = 'col';
        else:
            $xlw = $width['widescreen_desktop'];
            $lgw = $width['desktop'];
            $mdw = $width['tablet'];
            $smw = $width['phone_landscape'];
            $xsw = $width['phone_portrait'];
            if( $xsw ) $xs = ' col-'.$xsw; else $xs = 'col';
            if( $smw && $smw != $xsw ) $sm = ' col-sm-'.$smw; elseif( $smw == 0 ) $sm = ' col-sm';
            if( $mdw && ((!$sm && $mdw != $xsw) || ($sm && $mdw != $smw) )) $md = ' col-md-'.$mdw; elseif( $mdw == 0 ) $md = ' col-md';
            if( $lgw && ((!$md && $lgw != $xsw) || (!$md && $lgw != $smw) || ($md && $lgw != $mdw) )) $lg = ' col-lg-'.$lgw; elseif( $lgw == 0 ) $lg = ' col-lg';
            if( $xlw && ((!$lg && $xlw != $xsw) || (!$lg && $xlw != $smw) || (!$lg && $xlw != $mdw) || ($lg && $xlw != $lgw)) ) $xl = ' col-xl-'.$xlw; elseif( $xlw == 0 ) $xl = ' col-xl';
            $col = ltrim($xs.$sm.$md.$lg.$xl);
        endif;
        if( $classes ) $col = $classes.' '.$col;
        $colclasses = explode(" ", $col);
		$colclasses = apply_filters('flexible_columns_col_class', $colclasses, get_row(true));
        $colclasses = implode(' ', $colclasses);
        return $colclasses;
    }

    function animate($animate){
        $atype = strtolower($animate[0]['type']);
        $animate_data = false;
        if( $atype ):
            $dir = strtolower($animate[0][$atype.'_direction']);
            $dur = $animate[0]['duration']*1000;
            $delay = $animate[0]['delay']*1000;
            $animate_data = ' data-aos="'.$atype.'-'.$dir.'" data-aos-duration="'.$dur.'" data-aos-delay="'.$delay.'" ';
        endif;
        return $animate_data;
    }

    function list_item_columns($licols, $content){
        $listyle = false;
		if( $licols != 1){ 
			$listyle = 'li-col-'.$licols;
		}
        if( $listyle ) 
          $content = preg_replace('/<([uo]l)(\\sclass="([a-z0-9.])*'.$listyle.'([a-z0-9.])*")*>/m', '<$1 class="$3'.$listyle.'$4">', $content);
        return $content;
    }

    function order_classes($order){
        $xl = ''; $lg = ''; $md = ''; $sm = ''; $xs = '';
        $xlo = $order['xl'];
        $lgo = $order['lg'];
        $mdo = $order['md'];
        $smo = $order['sm'];
        $xso = $order['xs'];
        if( $xso ) $xs = ' order-'.$xso; 
        if( $smo && $smo != $xso ) $sm = ' order-sm-'.$smo;
        if( $mdo && ((!$sm && $mdo != $xso) || ($sm && $mdo != $smo) )) $md = ' order-md-'.$mdo; 
        if( $lgo && ((!$md && $lgo != $xso) || (!$md && $lgo != $smo) || ($md && $lgo != $mdo) )) $lg = ' order-lg-'.$lgo; 
        if( $xlo && ((!$lg && $xlo != $xso) || (!$lg && $xlo != $smo) || (!$lg && $xlo != $mdo) || ($lg && $xlo != $lgo)) ) $xl = ' order-xl-'.$xlo; 
        $classes = ltrim($xs.$sm.$md.$lg.$xl);
        
        return $classes;
    }

    function offset_classes($offset){
        $xl = ''; $lg = ''; $md = ''; $sm = ''; $xs = '';
        $xlo = $offset['xl'];
        $lgo = $offset['lg'];
        $mdo = $offset['md'];
        $smo = $offset['sm'];
        $xso = $offset['xs'];
        if( $xso ) $xs = ' offset-'.$xso; 
        if( $smo && $smo != $xso ) $sm = ' offset-sm-'.$smo;
        if( $mdo && ((!$sm && $mdo != $xso) || ($sm && $mdo != $smo) )) $md = ' offset-md-'.$mdo; 
        if( $lgo && ((!$md && $lgo != $xso) || (!$md && $lgo != $smo) || ($md && $lgo != $mdo) )) $lg = ' offset-lg-'.$lgo; 
        if( $xlo && ((!$lg && $xlo != $xso) || (!$lg && $xlo != $smo) || (!$lg && $xlo != $mdo) || ($lg && $xlo != $lgo)) ) $xl = ' offset-xl-'.$xlo; 
        $classes = ltrim($xs.$sm.$md.$lg.$xl);
        
        return $classes;
    }


    function column_classes(){
        $order = $this->order_classes(get_sub_field('order'));    
        $offset = $this->offset_classes(get_sub_field('offset'));  
        if( $order )   $class[] = $order;
        if( $offset ) $class[] = $offset;
        if( get_sub_field('column_class') ) $class[] = get_sub_field('column_class');
        if( get_sub_field('column_padding') ){
            $padding = get_sub_field('column_padding');
            $class[] = $this->padding_classes($padding);
        }
       // $additional_classes = get_sub_field('column_order').' '.get_sub_field('column_class').' '.$order.' '.$offset;           
        $class[] = $this->column_widths(get_sub_field('column_width'));
        return implode(' ', $class);
    }

    function padding_classes($pad){
        $padding = '';
        if( $pad['top'] == $pad['bottom'] && $pad['left'] == $pad['right'] && $pad['top'] == $pad['left'] && $pad['top'] != 'auto'){
            $padding .= 'p-'.$pad['top'];
        }
        if( $pad['top'] == $pad['bottom'] && $pad['left'] == $pad['right'] && $pad['top'] != $pad['left'] ){
            if( $pad['top'] != 'auto' ) $padding .= 'py-'.$pad['top'];
            if( $pad['left'] != 'auto' ) $padding .= ' px-'.$pad['left'];
        }
        if( $pad['top'] == $pad['bottom'] && $pad['left'] != $pad['right'] && ($pad['top'] != $pad['left'] || $pad['top'] != $pad['right']) ){
            if( $pad['top'] != 'auto' ) $padding .= 'py-'.$pad['top'];
            if( $pad['left'] != 'auto' ) $padding .= ' pl-'.$pad['left'];
            if( $pad['right'] != 'auto' ) $padding .= ' pr-'.$pad['right'];
        }
        if( $pad['top'] != $pad['bottom'] && $pad['left'] == $pad['right'] && ($pad['top'] != $pad['left'] || $pad['bottom'] != $pad['left']) ){
            if( $pad['top'] != 'auto' ) $padding .= 'pt-'.$pad['top'];
            if( $pad['bottom'] != 'auto' ) $padding .= ' pb-'.$pad['bottom'];
            if( $pad['left'] != 'auto' ) $padding .= ' px-'.$pad['left'];
        }
        if( $pad['top'] != $pad['bottom'] && $pad['left'] != $pad['right'] ){
            if( $pad['top'] != 'auto' ) $padding .= 'pt-'.$pad['top'];
            if( $pad['bottom'] != 'auto' ) $padding .= ' pb-'.$pad['bottom'];
            if( $pad['left'] != 'auto' ) $padding .= ' pl-'.$pad['left'];
            if( $pad['right'] != 'auto' ) $padding .= ' pr-'.$pad['right'];
        }
        return $padding;
    }
	
	function flex_col_layout($type){
        $columndata = '';
        $opts = get_option('acf_flexcol_opts');
        if( array_key_exists('disablecontentlayout', $opts) && $opts['disablecontentlayout'] ){
            switch($type):	
                default:
                $columndata .= apply_filters('flex_col_layout', $type);    
            endswitch;
        }else{
            switch($type):	
            #CONTENT
                case 'content':

                    $content = get_sub_field('content');
                    
                    $colclasses = $this->column_classes();
                    
                    $animate_data = $this->animate(get_sub_field('animate'));               
                    
                    $content = $this->list_item_columns(get_sub_field('list_layout'), $content);
                    
                    $columndata .= '<div class="'.$colclasses.'"'.$animate_data.'>' . do_shortcode($content) .'</div>';
                break;
                
            default:
                $columndata .= apply_filters('flex_col_layout', $type);
                
            endswitch;
        }
		return $columndata;
    }
    
    function flex_columns_admin_css(){
        echo '
        <style type="text/css">
        .acf-field-574881bf81a25 > .acf-input > .acf-flexible-content > .values > .layout > .acf-fields{
            display:flex;
            flex-wrap:wrap;
        }
        .acf-field-574881bf81a25 > .acf-input > .acf-flexible-content > .values > .layout > .acf-fields > .acf-field{
            flex: 1;
        }
        .acf-field-574881bf81a25 > .acf-input > .acf-flexible-content > .values > .layout > .acf-fields > .acf-tab-wrap{
            width:100%;
            flex: unset;
        }
        </style>
        ';
    }

    function container_classes($container_options){
        $size = $container_options['size'];
        //Container Class
        if( $size ) $class[] = 'container'; else $class[] = 'container-fluid';

        // Padding Classes
        if( array_key_exists('padding_options',$container_options) ){  //backwards compatibility
            $class[] = $container_options['padding_options'];
        }elseif( array_key_exists('padding',$container_options) ){ //new padding system
            $pad = $container_options['padding'];
            $class[] = $this->padding_classes($pad);
        }

        //Background Color
        $class[] = $container_options['background_color'];

        //Custom Classes
        if( array_key_exists('additional_classes',$container_options) && $container_options['additional_classes'] ) $class[] = $container_options['additional_classes'];

        $classes = implode(' ',$class);
        return $classes;
    }

    function container_options_wrap_outer(){
        $contain = '';
        if ( get_row_layout() == 'full_width_row' || get_row_layout() == '2_column_row' || get_row_layout() == '3_column_row' || get_row_layout() == '4_column_row' ){
            $container_options = get_sub_field('container_options');
            $class = $this->container_classes($container_options);
        
            if(  have_rows('column_1_block') || have_rows('column_2_block') || have_rows('column_3_block') || have_rows('column_4_block')):
        
                while( have_rows('column_1_block') || have_rows('column_2_block') || have_rows('column_3_block') || have_rows('column_4_block') ): the_row();
     
                    $contain = '<div class="'.$class.'">';

                endwhile; 
            endif; //END SINGLE COLUMN
        }
        return $contain;	
    }
    function container_options_wrap_outer_end(){
        $contain = '';
        if ( get_row_layout() == 'full_width_row' || get_row_layout() == '2_column_row' || get_row_layout() == '3_column_row' || get_row_layout() == '4_column_row' ){
            if( have_rows('column_1_block') || have_rows('column_2_block') || have_rows('column_3_block') || have_rows('column_4_block') ):
                while( have_rows('column_1_block') || have_rows('column_2_block') || have_rows('column_3_block') || have_rows('column_4_block') ): the_row();
                        
                    $contain = '</div>';		
                    
                endwhile; 
             endif; //END SINGLE COLUMN
        }
        return $contain;	
    }

}

$idp_acf_flexcols = new IDP_ACF_FlexCols();