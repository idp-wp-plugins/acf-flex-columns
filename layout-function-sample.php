<?php
/*
Add new content blocks within the Advanced Custom Fields, "Flex Columns - Block" Field Group by duplicating the Content layout within the Column Flexible Content field.  Adjust the fields as needed for your new field group. Then copy or include this file into your functions.php and add your layout types within the function below:
*/
//Add Layouts Here
function acf_fc_custom_layouts($type){
	global $idp_acf_flexcols;
	switch( $type ):
		case 'new_layout_field':
			$colclasses = $idp_acf_flexcols->column_classes();
			$animate_data = $idp_acf_flexcols->animate(get_sub_field('animate'));  
			$new_field = get_sub_field('new_field_name');
			$layout .= '<div class="'.$colclasses.'"'.$animate_data.'>' . $new_field .'</div>';
		break;
	endswitch;

    return $layout;
}
add_filter( 'flex_col_layout', 'acf_fc_custom_layouts');

//Add Row Classes
add_filter('flexible_columns_row_class', 'IDP_row_class');
function IDP_row_class($rowclasses){
	if ( get_row_layout() == 'full_width_row' || get_row_layout() == '2_column_row' || get_row_layout() == '3_column_row' || get_row_layout() == '4_column_row' ){
		$container_options = get_sub_field('column_class');
		$newclasses = explode(' ',$container_options['additional_classes']);
	}
	if( $newclasses ) $rowclasses = array_merge($rowclasses,$newclasses);
	return $rowclasses;
}

/* OPTIONAL CONTAINER OPTIONS */
//Modify Container Wrapper - opening tag
add_filter( 'flexible_columns_wrap_outer', 'IDP_container_options_wrap_outer',12);
function IDP_container_options_wrap_outer(){
	global $idp_acf_flexcols;
	$contain = '';
	if ( get_row_layout() == 'full_width_row' || get_row_layout() == '2_column_row' || get_row_layout() == '3_column_row' || get_row_layout() == '4_column_row' ){
		$container_options = get_sub_field('container_options');
		$classes = $idp_acf_flexcols->container_classes($container_options);
	
		if(  have_rows('column_1_block') || have_rows('column_2_block') || have_rows('column_3_block') || have_rows('column_4_block')):
	
			while( have_rows('column_1_block') || have_rows('column_2_block') || have_rows('column_3_block') || have_rows('column_4_block') ): the_row();
 
				$contain = '<div class="'.$classes.'">';

			endwhile; 
		endif; //END SINGLE COLUMN
	}
	return $contain;	
}
//Modify closing tag of container wrapper
add_filter( 'flexible_columns_wrap_outer_end', 'IDP_container_options_wrap_outer_end',12);
function IDP_container_options_wrap_outer_end(){
	$contain = '';
	if ( get_row_layout() == 'full_width_row' || get_row_layout() == '2_column_row' || get_row_layout() == '3_column_row' || get_row_layout() == '4_column_row' ){
		if( have_rows('column_1_block') || have_rows('column_2_block') || have_rows('column_3_block') || have_rows('column_4_block') ):
			while( have_rows('column_1_block') || have_rows('column_2_block') || have_rows('column_3_block') || have_rows('column_4_block') ): the_row();
					
				$contain = '</div>';		
				
			endwhile; 
		 endif; //END SINGLE COLUMN
	}
	return $contain;	
}